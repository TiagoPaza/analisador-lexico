# Analisador Léximo
## Antes de começar:
#### 1. Verificar se você possui instalado o PHP, Composer e o Git, caso não possua um dos dois:
    
    Baixar o Xampp(PHP7 ou superior): https://www.apachefriends.org/xampp-files/7.1.24/xampp-win32-7.1.24-0-VC14-installer.exe

    Baixar o Composer: https://getcomposer.org/Composer-Setup.exe

    Baixar o Git: https://git-scm.com/download/

## Inicializando o projeto:
#### 1. Criar uma nova pasta em um local de sua escolha.

#### 2. Acessar a pasta que foi criada via terminal e realizar o comando:
     
    git clone https://TiagoPaza@bitbucket.org/TiagoPaza/api-graphql.git

#### 3. Abrir a pasta via terminal ou IDE e realizar os seguintes comandos no prompt: 
    
    composer install (Irá baixar todos os pacotes do Node de acordo com o arquivo 'composer.json')
   
    php -S localhost:8000 (Comando que irá rodar a build e iniciar o projeto)

#### 4. Visualizar e testar:
    
    Acesse no navegador: http://localhost:8000/

- Divirta-se :green_heart: