<?php

namespace LexicalAnalyzer;

class Dictionary {

    /**
     * @var array
     */
    private $words = [];

    public function __construct(array $words = []) 
    {        
        $this->words = $words;
    }

    public function add(string $word) 
    {
        if (!$this->exists($word) && !empty($word)) {
            $this->words[] = $word;

            sort($this->words);
        }
    }

    public function remove(string $word): bool 
    {
        if (!$this->exists($word)) {
            return false;
        }

        unset($this->words[array_search($word, $this->words)]);
        sort($this->words);

        return true;
    }

    public function exists(string $word): bool 
    {
        return in_array($word, $this->words);
    }

    public function toArray(): array 
    {
        return $this->words;
    }

    public function asTokenizer(): Tokenizer 
    {
        $tokens = !empty($this->words) ? implode(' ', $this->words) . ' ' : '';
        $tokenizer = new Tokenizer($tokens);

        return $tokenizer;
    }
    
}
