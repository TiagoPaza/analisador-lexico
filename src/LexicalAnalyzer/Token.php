<?php

namespace LexicalAnalyzer;

class Token {

    /**
     *
     * @var string
     */
    private $token;
    
    /**
     *
     * @var string
     */
    private $trueToken;

    /**
     *
     * @var array
     */
    private $chars;

    public function __construct(string $token) 
    {
        $this->token = str_replace(Tokenizer::$epsylon, '', $token);
        $this->trueToken = $token;
        $this->chars = str_split(mb_strtoupper($this->token));
    }

    public function __toString(): string 
    {
        return $this->token;
    }

    public function toArray(): array 
    {
        return $this->chars;
    }

    public function isComplete(): bool 
    {
        $isComplete = strpos(
            $this->trueToken, 
            Tokenizer::$epsylon
        );
        
        return ($isComplete !== false);
    }

}
