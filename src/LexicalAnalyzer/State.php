<?php

namespace LexicalAnalyzer;

class State implements \ArrayAccess {
    
    /**
     * The index of the state in the FiniteAutomaton
     * @var int
     */
    private $index;
    
    /**
     * Associative array that determinizes the rules of the state
     * @var array
     */
    private $rules;
    
    /**
     * Identify the state as Finish
     * @var bool
     */
    private $isFinish = true;
    
    public function __construct(int $index = 0, array $rules = []) 
    {
        $this->index = $index;
        $this->setRules($rules);
    }

    public function hasRule(string $char): bool 
    {
        return isset($this->rules[$char]);
    }
    
    public function addRule(string $char, int $stateTo): void 
    {
        $this->offsetSet($char, $stateTo);
    }
    
    public function getRule(string $char): int 
    {
        return $this->offsetGet($char);
    }
    
    public function getIndex(): int 
    {
        return $this->index;
    }

    public function getRules(): array 
    {
        return $this->rules;
    }

    public function setIndex(int $index) 
    {
        $this->index = $index;
        
        return $this;
    }

    public function setRules(array $rules) 
    {
        $this->rules = $rules;
        
        if(!empty($this->rules)) {
            $this->isFinish = false;
        }
        
        return $this;
    }

    public function isFinish(): bool 
    {
        return $this->isFinish;
    }

    public function offsetExists($char): bool 
    {
        return isset($this->rules[$char]);
    }

    public function offsetGet($char) 
    {
        if(!isset($this->rules[$char])) {
            throw new \InvalidArgumentException(
                    "There's no rule for '$char' at state of index $this->index");
        }
        
        return $this->rules[$char];
    }

    public function offsetSet($char, $stateTo): void 
    {
        $this->rules[$char] = $stateTo;
        $this->isFinish = false;
    }


    public function offsetUnset($char): void 
    {
        unset($this->rules[$char]);
        
        if(empty($this->rules)) {
            $this->isFinish = true;
        }
    }

}
