<?php

namespace LexicalAnalyzer\Traits;

trait LexicalAnalyzerResponder {
    
    protected function formatReadResponse($element) 
    {
        switch ($element->valid) {
            case true:
                $element->valid = 'text-success';
                break;
            case false:
                $element->valid = 'text-danger';
                break;
            default:
                $element->valid = null;
                break;
        }

        return $element;
    }

}
