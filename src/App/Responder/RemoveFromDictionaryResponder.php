<?php

namespace App\Responder;

use Slim\Http\Response;

class RemoveFromDictionaryResponder extends AbstractResponder {
    
    use \LexicalAnalyzer\Traits\LexicalAnalyzerResponder;

    public function __invoke(Response $response, array $data): Response 
    {
        $data['read'] = array_map([$this, 'formatReadResponse'], $data['read']);
        
        return $response->withJson($data);
    }

}
