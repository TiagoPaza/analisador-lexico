<?php

namespace App\Action;

use App\Domain\ResetDictionaryDomain;
use App\Responder\ResetDictionaryResponder;

class ResetDictionaryAction extends AbstractAction {

    public $domain = ResetDictionaryDomain::class;
    public $responder = ResetDictionaryResponder::class;

}
