<?php

namespace App\Action;

use App\Domain\ActualStateDomain;
use App\Responder\ActualStateResponder;

class ActualStateAction extends AbstractAction {
    
    public $domain = ActualStateDomain::class;
    public $responder = ActualStateResponder::class;

}
