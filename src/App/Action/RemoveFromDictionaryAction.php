<?php

namespace App\Action;

use App\Domain\RemoveFromDictionaryDomain;
use App\Responder\RemoveFromDictionaryResponder;

class RemoveFromDictionaryAction extends AbstractAction {

    public $domain = RemoveFromDictionaryDomain::class;    
    public $responder = RemoveFromDictionaryResponder::class;
    
}
