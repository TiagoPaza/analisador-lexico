<?php

namespace App\Action;

use App\Domain\BeginAnalyzerDomain;
use App\Responder\BeginAnalyzerResponder;

class BeginAnalyzerAction extends AbstractAction {

    public $domain = BeginAnalyzerDomain::class;    
    public $responder = BeginAnalyzerResponder::class;   

}
