<?php

namespace App\Action;

use App\Domain\ReadFromDictionaryDomain;
use App\Responder\ReadFromDictionaryResponder;

class ReadFromDictionaryAction  extends AbstractAction{
    
    public $domain = ReadFromDictionaryDomain::class;
    public $responder = ReadFromDictionaryResponder::class;

}
