<?php

namespace App\Action;

use App\Domain\ListDictionaryDomain;
use App\Responder\ListDictionaryResponder;

class ListDictionaryAction extends AbstractAction {
    
    public $domain = ListDictionaryDomain::class;    
    public $responder = ListDictionaryResponder::class;
    
}
