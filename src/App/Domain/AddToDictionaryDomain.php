<?php

namespace App\Domain;

use Slim\Http\Request;
use LexicalAnalyzer\Tokenizer;

class AddToDictionaryDomain extends AbstractDomain {
    
    /**
     * @param Request $request
     * @return array
     */
    public function __invoke(Request $request): array 
    {    
        $analyzer = $this->container->get('analyzer');
        $body = $request->getParsedBody();
        
        $tokenizer = new Tokenizer($body['in']);
        $analyzer->addWord($tokenizer->end());
        
        $read = $analyzer->readInput($tokenizer);
        
        $analyzer->saveState($this->container->get('storage'));
        
        return compact('read');
    }

}
