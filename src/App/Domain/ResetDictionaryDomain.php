<?php

namespace App\Domain;

use Slim\Http\Request;

class ResetDictionaryDomain extends AbstractDomain {
    
    /**
     * @param Request $request
     * @return array
     */
    public function __invoke(Request $request): array 
    {    
        \SlimSession\Helper::destroy();
        
        return ['session' => 'destroyed'];
    }

}
