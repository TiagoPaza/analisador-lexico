<?php

namespace App\Domain;

use Slim\Http\Request;
use LexicalAnalyzer\Tokenizer;
use LexicalAnalyzer\Token;

class RemoveFromDictionaryDomain extends AbstractDomain {
    
    /**
     * @param Request $request
     * @return array
     */
    public function __invoke(Request $request): array 
    {    
        $analyzer = $this->container->get('analyzer');
        $analyzer->removeWord(new Token($request->getParsedBodyParam('word')));

        $read = $analyzer->readInput(new Tokenizer($request->getParsedBodyParam('in')));
        $analyzer->saveState($this->container->get('storage'));

        return compact('read');
    }

}
