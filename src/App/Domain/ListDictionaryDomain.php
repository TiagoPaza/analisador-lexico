<?php

namespace App\Domain;

use Slim\Http\Request;

class ListDictionaryDomain extends AbstractDomain {
    
    /**
     * @param Request $request
     * @return array
     */
    public function __invoke(Request $request): array 
    {
        $analyzer = $this->container->get('analyzer');
        $dictionary = $analyzer->getAutomaton()->dictionary;

        return compact('dictionary');
    }

}
