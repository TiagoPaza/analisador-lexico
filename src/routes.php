<?php

use App\Action\
{
    BeginAnalyzerAction, ReadFromDictionaryAction, ActualStateAction, ResetDictionaryAction, 
    AddToDictionaryAction, ListDictionaryAction, RemoveFromDictionaryAction
};

$app->get('/', BeginAnalyzerAction::class)->setName('begin');
$app->get('/actual-state', ActualStateAction::class)->setName('actual-state');
$app->get('/dictionary', ListDictionaryAction::class)->setName('dictionary');

$app->put('/read', ReadFromDictionaryAction::class)->setName('read');

$app->patch('/dictionary/add', AddToDictionaryAction::class)->setName('add-word');

$app->delete('/reset', ResetDictionaryAction::class)->setName('reset');
$app->delete('/dictionary/remove', RemoveFromDictionaryAction::class)->setName('remove-word');